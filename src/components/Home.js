import React, { Component } from "react";
import "./Home.css";

export default class Home extends Component {
    render() {
        return (
            <div className="Home">
                <div className="lander">
                    <h1>CMS</h1>
                    <p>Company management system</p>
                </div>
            </div>
        );
    }
}
