import React, {Component} from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import NotFound from "./NotFound";
import Login from "./Login";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Calendar from "./Calendar";
import NewEvent from "./NewEvent";
import Vacation from "./Vacation";
import WorkRegister from "./WorkRegister";
import AdminPanel from "./AdminPanel";

const PrivateRoute = ({ component: Component, authenticated, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
            if (authenticated) {
                return <Component {...props} />
            } else {
                return <Redirect to='/login' />
            }
        }}
    />
)

const LoginRoute = ({ component: Component, authenticated, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
            if (authenticated) {
                return <Redirect to='/calendar' />
            } else {
                return <Component {...props} />
            }
        }}
    />
)

class Routes extends Component {

    render() {
        return (
            <div style={{marginTop: '80px'}}>
                <Switch>
                    <LoginRoute authenticated={this.props.logged} exact path="/login" component={Login} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/" component={Calendar} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/calendar" component={Calendar} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/event" component={NewEvent} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/vacation" component={Vacation} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/registerWork" component={WorkRegister} />
                    <PrivateRoute authenticated={this.props.logged} exact path="/adminPanel" component={AdminPanel} />
                    <PrivateRoute authenticated={this.props.logged} exact component={NotFound} />
                </Switch>
            </div>
        )
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

const mapStateToProps = ({ logged }) => ({
    logged: logged.logged
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Routes));