import React, { Component } from "react";
import "./App.css";
import style from 'bootstrap/dist/css/bootstrap.css';
import Routes from "./Routes";
import NavBar from "./NavBar";
import LoadingBlocker from "./LoadingBlocker";
import {Notifs} from "redux-notifications";

class App extends Component {

    render() {
        return (
            <LoadingBlocker>
                <div className="App container">
                    <NavBar/>
                    <Notifs/>
                    <Routes/>
                </div>
            </LoadingBlocker>
        );
    }
}

export default App;