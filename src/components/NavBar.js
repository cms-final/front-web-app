import React from 'react'
import {push} from 'connected-react-router'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {showInfoMessage} from "../reducers/globalMessages";
import {logout} from "../reducers/logged";
import {logoutSuccessfulMessage} from "../constants/messages";

function logoutRequest(props) {
    props.logout()
    props.showInfoMessage(logoutSuccessfulMessage)
}

const NavBar = props => (
    <Navbar fluid collapseOnSelect>
        <Navbar.Header>
            <Navbar.Brand>
                CMS
            </Navbar.Brand>
        </Navbar.Header>
        <Navbar.Collapse>
            {props.logged &&
            <Nav pullRight>
                <NavItem onClick={() => props.goToCalendar()}>
                    Calendar
                </NavItem>
                <NavItem onClick={() => props.goToEvent()}>
                    Event
                </NavItem>
                <NavItem onClick={() => props.goToVacation()}>
                    Vacation
                </NavItem>
                <NavItem onClick={() => props.goToRegisterWork()}>
                    Work register
                </NavItem>
                <NavItem onClick={() => props.goToAdminPanel()}>
                    Admin panel
                </NavItem>
                <NavItem onClick={() => logoutRequest(props)}>
                    Log out
                </NavItem>
            </Nav>
            }
        </Navbar.Collapse>
    </Navbar>
)

const mapDispatchToProps = dispatch => bindActionCreators({
    goToCalendar: () => push('/calendar'),
    goToEvent: () => push('/event'),
    goToVacation: () => push('/vacation'),
    goToRegisterWork: () => push('/registerWork'),
    goToAdminPanel: () => push('/adminPanel'),
    showInfoMessage,
    logout
}, dispatch)

const mapStateToProps = ({ logged }) => ({
    logged: logged.logged
})

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)

