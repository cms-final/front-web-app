import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {showErrorMessage, showInfoMessage, showLoadingBlocker, showSuccessMessage} from "../reducers/globalMessages";
import {notifSend} from "redux-notifications/src/actions";
import connect from "react-redux/es/connect/connect";
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import {withStyles} from '@material-ui/core/styles';
import "react-datepicker/dist/react-datepicker.css";
import config from "../constants/config";
import Select from 'react-select';
import axios from "axios";


const styles = () => ({
    container: {
        paddingTop: 20,
        maxHeight: 1000,
        width: 500,
        display: 'flex',
        flexWrap: 'wrap',
    },
    formGroup: {
        width: 400,
    },
    submit: {
        paddingTop: 70,
        width: 250,
    },
    datePicker: {
        width: 220,
        height: 35,
    },
    multiSelect: {
        minHeight: 150,
    },
});

const eventTypes = [
    { value: 'TRAINING', label: 'Training' },
    { value: 'MEETING', label: 'Meeting' },
    { value: 'CONFERENCE', label: 'Conference' },
    { value: 'DELEGATION', label: 'Delegation' },
];

class NewEvent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: '',
            type: '',
            startDate: new Date(new Date(new Date().setHours(13)).setMinutes(0)),
            endDate: new Date(new Date(new Date().setHours(14)).setMinutes(0)),
            usernames: [],
            groupNames: [],
            users: [],
            groups: [],
        };
    }

    componentDidMount() {
        this.props.showLoadingBlocker(true);
        this.getUsernames();
        this.getGroupNames();
    }

    handleTypeChange = event => {
        this.setState({
            type: event.value
        })
    };

    handleUsersChange = event => {
        this.setState({
            users: event.map(e => e.value)
        })
    };

    handleGroupsChange = event => {
        this.setState({
            groups: event.map(e => e.value)
        })
    };

    handleStartDateChange = event => {
        this.setState({
            startDate: event
        })
    };

    handleEndDateChange = event => {
        this.setState({
            endDate: event
        })
    };

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        })
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.showLoadingBlocker(true);
        let body = {
            title: this.state.title,
            description: this.state.description,
            type: this.state.type,
            startDate: this.state.startDate.toISOString(),
            endDate: this.state.endDate.toISOString(),
            users: this.state.users,
            groups: this.state.groups,
        };
        axios.post(config.eventAddress, body, {
        }).then(response => {
            this.props.showLoadingBlocker(false);
            this.props.showSuccessMessage('Successfully created event: ' + response.data.title);
        });
    };

    isFormValid = () => {
        return this.state.title.length > 0
            && this.state.type.length > 0
            && this.state.startDate >= new Date()
            && this.state.startDate < this.state.endDate
            && (this.state.users.length > 0 || this.state.groups.length > 0)
    };

    getUsernames = () => {
        axios.get(config.usersAddress + 'usernames')
            .then(response => {
                if (response.data.length === 0) {
                    this.props.showLoadingBlocker(false);
                    this.props.showErrorMessage('Empty usernames select');
                }
                this.setState({
                    usernames: response.data.map(username => {
                        return {'value': username, 'label': username};
                    })
                });
            });
    };

    getGroupNames = () => {
        axios.get(config.usersAddress + 'groupNames')
            .then(response => {
                if (response.data.length === 0) {
                    this.props.showLoadingBlocker(false);
                    this.props.showErrorMessage('Empty group names select');
                }
                this.setState({
                    groupNames: response.data.map(groupName => {
                        return {'value': groupName, 'label': groupName};
                    })
                });
            });
    };

    render() {
        if (this.state.usernames.length !== 0 && this.state.groupNames.length !== 0) {
            //todo: redux
            this.props.showLoadingBlocker(false);
        }

        const { classes } = this.props;

        return (
            <div>
                <h2>Create new event</h2>
                <form className={classes.container} autoComplete="off" onSubmit={this.handleSubmit}>
                    <FormGroup controlId="title" className={classes.formGroup}>
                        <ControlLabel>Title</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.title}
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="description" className={classes.formGroup}>
                        <ControlLabel>Description</ControlLabel>
                        <FormControl
                            componentClass="textarea"
                            value={this.state.description}
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="eventType" className={classes.formGroup}>
                        <ControlLabel>Type</ControlLabel>
                        <Select
                            placeholder=''
                            onChange={this.handleTypeChange}
                            autosize={false}
                            value={this.state.eventType}
                            options={eventTypes}
                        />
                    </FormGroup>

                    <FormGroup controlId="startDate" className={classes.formGroup}>
                        <ControlLabel>Start</ControlLabel>
                        <br/>
                        <DatePicker
                            className={classes.datePicker}
                            selected={this.state.startDate}
                            onChange={this.handleStartDateChange}
                            dropdownMode="select"
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </FormGroup>

                    <FormGroup controlId="endDate" className={classes.formGroup}>
                        <ControlLabel>End</ControlLabel>
                        <br/>
                        <DatePicker
                            className={classes.datePicker}
                            selected={this.state.endDate}
                            onChange={this.handleEndDateChange}
                            dropdownMode="select"
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </FormGroup>

                    <FormGroup controlId="usersSelect" className={classes.formGroup}>
                        <ControlLabel>Users</ControlLabel>
                        <Select
                            isMulti={true}
                            onChange={this.handleUsersChange}
                            value={this.state.usersSelect}
                            options={this.state.usernames}
                        />
                    </FormGroup>

                    <FormGroup controlId="groupsSelect" className={classes.formGroup}>
                        <ControlLabel>Users</ControlLabel>
                        <Select
                            isMulti={true}
                            onChange={this.handleGroupsChange}
                            value={this.state.groupsSelect}
                            options={this.state.groupNames}
                        />
                    </FormGroup>

                    <FormGroup controlId="submit" className={classes.submit}>
                        <Button
                            block
                            bsSize="large"
                            disabled={!this.isFormValid()}
                            type="submit"
                        >
                            Submit
                        </Button>
                    </FormGroup>
                </form>
            </div>
        );
    }
}

NewEvent.propTypes = {
    classes: PropTypes.object.isRequired,
};


const mapDispatchToProps = dispatch => bindActionCreators({
    showLoadingBlocker,
    showSuccessMessage,
    showInfoMessage,
    showErrorMessage,
    notifSend
}, dispatch);

const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NewEvent))