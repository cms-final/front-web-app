import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {showErrorMessage, showInfoMessage, showLoadingBlocker, showSuccessMessage} from "../reducers/globalMessages";
import {notifSend} from "redux-notifications/src/actions";
import connect from "react-redux/es/connect/connect";
import {Button, ControlLabel, FormGroup} from "react-bootstrap";
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import {withStyles} from '@material-ui/core/styles';
import "react-datepicker/dist/react-datepicker.css";
import config from "../constants/config";
import axios from "axios";


const styles = () => ({
    container: {
        maxHeight: 1000,
        width: 500,
        display: 'flex',
        flexWrap: 'wrap',
    },
    formGroup: {
        width: 400,
    },
    submit: {
        paddingTop: 20,
        width: 250,
    },
    datePicker: {
        width: 220,
        height: 35,
    },
});

class Vacation extends Component {

    constructor(props) {
        super(props);

        this.state = {
            daysOffLeft: null,
            startDate: new Date(new Date(new Date().setHours(13)).setMinutes(0)),
            endDate: new Date(new Date(new Date().setHours(14)).setMinutes(0)),
        };
    }

    componentDidMount() {
        this.props.showLoadingBlocker(true);
        this.getDaysOffLeft();
    }

    handleStartDateChange = event => {
        this.setState({
            startDate: event
        })
    };

    handleEndDateChange = event => {
        this.setState({
            endDate: event
        })
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.showLoadingBlocker(true);
        let body = {
            startDate: this.state.startDate.toISOString(),
            endDate: this.state.endDate.toISOString(),
        };
        axios.post(config.vacationsAddress, body, {
        }).then(response => {
            this.props.showLoadingBlocker(false);
            this.props.showSuccessMessage('Successfully added vacation');
            this.componentDidMount();
        });
    };

    isFormValid = () => {
        return this.state.startDate >= new Date() && this.state.startDate < this.state.endDate
    };

    getDaysOffLeft = () => {
        axios.get(config.vacationsAddress + 'daysOffLeft')
            .then(response => {
                this.setState({
                    daysOffLeft: response.data
                });
                this.props.showLoadingBlocker(false);
            });
    };

    downloadReport = () => {
        axios({
            url: config.vacationsAddress + 'report',
            method: 'GET',
            responseType: 'blob',
        }).then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'report.pdf');
            document.body.appendChild(link);
            link.click();
        });
    };

    render() {
        const { classes } = this.props;

        return (
            <div>
                <h1>Vacations</h1>

                <div style={{paddingBottom: '40px'}}>
                    {this.state.daysOffLeft !== null && (<h4>Days off left: {this.state.daysOffLeft}</h4>)}
                </div>

                <div style={{width: '150px', paddingBottom: '40px'}}>
                    <Button
                        bsSize="medium"
                        type="button"
                        onClick={this.downloadReport}
                    >
                        Download report
                    </Button>
                </div>

                <h4>Add vacation</h4>
                <form className={classes.container} autoComplete="off" onSubmit={this.handleSubmit}>
                    <FormGroup controlId="startDate" className={classes.formGroup}>
                        <ControlLabel>Start</ControlLabel>
                        <br/>
                        <DatePicker
                            className={classes.datePicker}
                            selected={this.state.startDate}
                            onChange={this.handleStartDateChange}
                            dropdownMode="select"
                            dateFormat="d MMMM yyyy"
                        />
                    </FormGroup>

                    <FormGroup controlId="endDate" className={classes.formGroup}>
                        <ControlLabel>End</ControlLabel>
                        <br/>
                        <DatePicker
                            className={classes.datePicker}
                            selected={this.state.endDate}
                            onChange={this.handleEndDateChange}
                            dropdownMode="select"
                            dateFormat="d MMMM yyyy"
                        />
                    </FormGroup>

                    <FormGroup controlId="submit" className={classes.submit}>
                        <Button
                            block
                            bsSize="large"
                            disabled={!this.isFormValid()}
                            type="submit"
                        >
                            Submit
                        </Button>
                    </FormGroup>
                </form>
            </div>
        );
    }
}

Vacation.propTypes = {
    classes: PropTypes.object.isRequired,
};


const mapDispatchToProps = dispatch => bindActionCreators({
    showLoadingBlocker,
    showSuccessMessage,
    showInfoMessage,
    showErrorMessage,
    notifSend
}, dispatch);

const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Vacation))