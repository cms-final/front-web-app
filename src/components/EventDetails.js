import React from "react";
import {Button, Modal} from "react-bootstrap";

export default (eventDetails, isVisible, onHide) =>
    <Modal show={isVisible} onHide={onHide}>
        <Modal.Header closeButton>
            <Modal.Title>{eventDetails.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <b>Description</b>: {eventDetails.description}<br/>
            <b>Type</b>: {eventDetails.type}<br/>
            {/*<b>Date</b>: {eventDetails.startDate.toLocaleString()} - {eventDetails.endDate.toLocaleString()}<br/>*/}
        </Modal.Body>
        <Modal.Footer>
            <Button onClick={onHide}>Close</Button>
        </Modal.Footer>
    </Modal>;