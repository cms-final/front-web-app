import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {showErrorMessage, showInfoMessage, showLoadingBlocker, showSuccessMessage} from "../reducers/globalMessages";
import {notifSend} from "redux-notifications/src/actions";
import connect from "react-redux/es/connect/connect";
import {Button} from "react-bootstrap";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import "react-datepicker/dist/react-datepicker.css";
import config from "../constants/config";
import axios from "axios";
import Select from "react-select";


class AdminPanel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: null,
            usernames: [],
        };
    }

    componentDidMount() {
        this.props.showLoadingBlocker(true);
        this.getUsernames();
    }

    getUsernames = () => {
        axios.get(config.usersAddress + 'usernames')
            .then(response => {
                if (response.data.length === 0) {
                    this.props.showLoadingBlocker(false);
                    this.props.showErrorMessage('Empty usernames select');
                }
                this.setState({
                    usernames: response.data.map(username => {
                        return {'value': username, 'label': username};
                    })
                });
                this.props.showLoadingBlocker(false);
            });
    };

    deleteEvent = () => {
        this.props.showLoadingBlocker(true);
        let self = this;
        axios.delete(config.eventAddress + self.state.user)
            .then(() => {
                self.props.showLoadingBlocker(false);
                self.props.showSuccessMessage('Successfully deleted user: ' + this.state.eventDetails.title);
                this.componentDidMount();
            });
    };

    handleChange = event => {
        this.setState({
            type: event.value
        })
    };

    render() {
        const { classes } = this.props;

        return (
            <div>
                <div style={{paddingBottom: '40px'}}>
                    <h1>Admin panel</h1>
                </div>

                <h4>Delete user</h4>

                <div style={{width: '300px', paddingBottom: '10px'}}>
                    <Select
                        placeholder=''
                        onChange={this.handleChange}
                        value={this.state.filterUsersSelect}
                        options={this.state.usernames}
                    />
                </div>

                <Button
                    type="submit"
                    onClick={this.deleteEvent}
                >
                    Delete
                </Button>
            </div>
        );
    }
}

AdminPanel.propTypes = {
    classes: PropTypes.object.isRequired,
};


const mapDispatchToProps = dispatch => bindActionCreators({
    showLoadingBlocker,
    showSuccessMessage,
    showInfoMessage,
    showErrorMessage,
    notifSend
}, dispatch);

const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel)