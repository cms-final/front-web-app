import React, {Component} from "react";
import {Button, Checkbox, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import "./Register.css";
import * as AmazonCognitoIdentity from "amazon-cognito-identity-js";
import config from "../constants/config";
import {bindActionCreators} from "redux";
import {showErrorMessage, showInfoMessage, showLoadingBlocker, showSuccessMessage} from "../reducers/globalMessages";
import {loginSuccess} from "../reducers/logged";
import {notifSend} from "redux-notifications/src/actions";
import connect from "react-redux/es/connect/connect";
import Select from "react-select";
import {withStyles} from "@material-ui/core";
import {loginSuccessfulMessage} from "../constants/messages";
import {registerFirebase} from "../firebase";
import axios from "axios";

const styles = () => ({
    h2: {
        paddingBottom: 75,
        textAlign: 'center',
    },
    container: {
        paddingTop: 20,
        maxHeight: 1000,
        width: 500,
        display: 'flex',
        flexWrap: 'wrap',
    },
    formGroup: {
        width: 320,
    },
    submit: {
        paddingTop: 70,
        width: 250,
    },
});

const contractTypes = [
    { value: 'Full-time', label: 'Full-time' },
    { value: 'Part-time', label: 'Part-time' },
];

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            oldPassword: "",
            newPassword: "",
            contractType: "",
            daysOff: 0,
            hoursPerMonth: 0,
        };
    }

    validateForm() {
        return (
            this.state.username.length > 0
            && this.state.oldPassword.length > 0
            && this.state.newPassword.length > 0
            && this.state.contractType.length > 0
            && this.state.contractType === "Full-time" ? this.state.daysOff > 0 : this.state.hoursPerMonth > 0
        );
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleContractTypeChange = event => {
        this.setState({
            contractType: event.value
        })
    };

    handleSubmit = async event => {
        event.preventDefault();
        this.props.showLoadingBlocker(true);
        this.changePassword();
    };

    changePassword = () => {
        let poolData = {
            UserPoolId : config.cognito.USER_POOL_ID,
            ClientId : config.cognito.APP_CLIENT_ID
        };
        let userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        let userData = {
            Username : this.state.username,
            Pool : userPool
        };
        let authenticationData = {
            Username : this.state.username,
            Password : this.state.oldPassword
        };
        let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        let newPassword = this.state.newPassword;
        let self = this;
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                self.props.loginSuccess();
                self.props.showSuccessMessage(loginSuccessfulMessage);
                self.props.showLoadingBlocker(false);
                console.log(result);
                registerFirebase()
            },

            onFailure: function(err) {
                self.props.showLoadingBlocker(false);
                self.props.showErrorMessage(err.message);
                console.log(err);
            },

            newPasswordRequired: function(userAttributes, requiredAttributes) {
                let contractTypeAttribute = {
                    'custom:contractType' : self.state.contractType
                };

                let selfInside = self;
                cognitoUser.completeNewPasswordChallenge(newPassword, contractTypeAttribute, {
                    onFailure: function (err) {
                        selfInside.props.showLoadingBlocker(false);
                        selfInside.props.showErrorMessage(err.message);
                        console.log(err);
                    },

                    onSuccess: function (result) {
                        localStorage.setItem('username', selfInside.state.username);
                        selfInside.registerUser(result.getIdToken().getJwtToken());
                        console.log(result);
                        registerFirebase()
                    }
                });
            }
        });
    };

    registerUser = (jwt) => {
        localStorage.setItem("jwt", jwt);
        let url;
        let body;
        if (this.state.contractType === "Full-time") {
            url = config.vacationsAddress + 'userRegistration';
                body = {
                    daysOffQuantity: this.state.daysOff,
                };
            } else {
                url = config.workRegistersAddress + 'userRegistration';
                body = {
                    hoursPerMonth: this.state.hoursPerMonth,
                };
            }
            axios.post(url, body, {
            }).then(response => {
                this.props.loginSuccess(jwt);
                this.props.showLoadingBlocker(false);
                this.props.showSuccessMessage('Successfully registered and logged in!');
            }).catch(error => console.log(error));
    };
    
    render() {
        const { classes } = this.props;

        return (
            <div className="Register">
                <h2>It's your first time here! Please change your password and give us some additional information to registration</h2>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.username}
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="oldPassword" bsSize="large">
                        <ControlLabel>Old password</ControlLabel>
                        <FormControl
                            value={this.state.oldPassword}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>

                    <FormGroup controlId="newPassword" bsSize="large">
                        <ControlLabel>New Password</ControlLabel>
                        <FormControl
                            value={this.state.newPassword}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>

                    <FormGroup controlId="eventType" className={classes.formGroup}>
                        <ControlLabel>Type</ControlLabel>
                        <Select
                            placeholder=''
                            onChange={this.handleContractTypeChange}
                            autosize={false}
                            value={this.state.contractTypeSelect}
                            options={contractTypes}
                        />
                    </FormGroup>

                    {this.state.contractType !== "" &&
                        (this.state.contractType === 'Full-time' ?
                            <FormGroup controlId="daysOff" bsSize="large">
                                <ControlLabel>Days off</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.daysOff}
                                    onChange={this.handleChange}
                                />
                            </FormGroup>
                            :
                            <FormGroup controlId="hoursPerMonth" bsSize="large">
                                <ControlLabel>Hours per month</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.hoursPerMonth}
                                    onChange={this.handleChange}
                                />
                            </FormGroup>
                        )
                    }

                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Register
                    </Button>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    showLoadingBlocker,
    showSuccessMessage,
    showInfoMessage,
    showErrorMessage,
    loginSuccess,
    notifSend
}, dispatch);

const mapStateToProps = ({}) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Register))