import React from 'react'
import BlockUi from 'react-block-ui'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import 'react-block-ui/style.css';

const LoadingBlocker = props => (
    //todo: replace with redux
    <BlockUi
        tag="div"
        blocking={props.visible}
    >
        {props.children}
    </BlockUi>
)

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

const mapStateToProps = ({ globalMessages }) => ({
    visible: globalMessages.loadingBlockerVisible
})

export default connect(mapStateToProps, mapDispatchToProps)(LoadingBlocker)

