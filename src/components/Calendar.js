import React, {Component} from "react";
import BigCalendar from 'react-big-calendar-like-google'
import moment from 'moment'
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import 'react-big-calendar-like-google/lib/css/react-big-calendar.css'
import {Button, Modal} from "react-bootstrap";
import axios from "axios";
import config from "../constants/config";
import {showErrorMessage, showLoadingBlocker, showSuccessMessage} from "../reducers/globalMessages";
import Select from "react-select";
import {withStyles} from "@material-ui/core";

// const events = [
//     {
//         title: 'Conference',
//         allDay: true,
//         start: new Date(new Date().setHours(new Date().getHours() + 350)),
//         end: new Date(new Date().setHours(new Date().getHours() + 358)),
//     },
//     {
//         title: 'Meeting with client',
//         start: '2018-09-12',
//         // start: new Date(new Date().setHours(new Date().getHours() + 150)),
//         end: '2018-09-12',
//         // end: new Date(new Date().setHours(new Date().getHours() + 155)),
//     },
//     {
//         title: 'Days off!',
//         bgColor: '#1bcb3b',
//         start: new Date(new Date().setHours(new Date().getHours() - 45)),
//         end: new Date(new Date().setHours(new Date().getHours() - 24))
//     },
//
//     {
//         title: 'Security training',
//         start: new Date(new Date().setHours(new Date().getHours() + 700)),
//         end: new Date(new Date().setHours(new Date().getHours() + 710)),
//     },
//     {
//         title: 'Delegation',
//         start: new Date(new Date().setHours(new Date().getHours() + 250)),
//         end: new Date(new Date().setHours(new Date().getHours() + 265)),
//         desc: 'Big conference for important people',
//     }
// ];

const styles = () => ({
    Modal: {
        zIndex: 99999,
    },
    Select: {
        zIndex: 9999,
        width: '300px',
        display: 'inline-block',
        paddingRight: '25px',
    },
});

const filterTypes = [
    { value: 'users', label: 'Users' },
    { value: 'groups', label: 'Groups' },
];

const colorMap = {
    TRAINING:  '#3d87cb',
    MEETING:  '#3d87cb',
    CONFERENCE:  '#3d87cb',
    DELEGATION:  '#3d87cb',
    VACATION: '#0db415',
    WORKING_DAY: '#a80604',
};

const localizer = BigCalendar.momentLocalizer(moment);

function displayEvent(event) {
    return {
        id: event.id,
        title: event.title,
        bgColor: colorMap[event.type],
        desc: event.description,
        start: new Date(event.startDate),
        end: new Date(event.endDate)
    }
}

class Calendar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showDetails: false,
            events: [],
            eventDetails: {},
            filterType: "",
            filterValue: "",
            usernames: [],
            groupNames: [],
        };
    }

    componentDidMount() {
        this.props.showLoadingBlocker(true);
        this.getAllEvents();
        this.getUsernames();
        this.getGroupNames();

    }

    handleCloseEvent = () => {
        this.setState({
            showDetails: false,
            eventDetails: {}
        })
    };

    handleShowEvent = event => {
        this.props.showLoadingBlocker(true);
        let self = this;
        axios.get(config.eventAddress + event.id)
            .then(response => {
                self.setState({
                    showDetails: true,
                    eventDetails: {
                        id: response.data.id,
                        title: response.data.title,
                        type: response.data.type,
                        description: response.data.description,
                        startDate: new Date(response.data.startDate),
                        endDate: new Date(response.data.endDate),
                        users: response.data.users,
                        groups: response.data.groups,
                        deletionAllowed: response.data.deletionAllowed,
                    }
                });
                self.props.showLoadingBlocker(false);
            });
    };

    handleFilterTypeChange = event => {
        this.setState({
            filterType: event.value,
            filterValue: ''
        })
    };

    handleFilterValueChange = event => {
        let self = this;
        self.props.showLoadingBlocker(true);
        self.setState({
            filterValue: event.value
        });
        axios.get(config.eventAddress + self.state.filterType + '/' + event.value)
            .then(response => {
                self.setState({
                    events: response.data.map(event => displayEvent(event))
                });
                self.props.showLoadingBlocker(false);
            });
    };

    deleteEvent = () => {
        this.props.showLoadingBlocker(true);
        let self = this;
        axios.delete(config.eventAddress + self.state.eventDetails.id)
            .then(() => {
                self.handleCloseEvent();
                self.props.showLoadingBlocker(false);
                self.props.showSuccessMessage('Successfully deleted event: ' + this.state.eventDetails.title);
                this.componentDidMount();
            });
    };

    getAllEvents = () => {
        let self = this;
        axios.get(config.eventAddress)
            .then(response => {
                self.setState({
                    events: response.data.map(event => displayEvent(event))
                });
                self.props.showLoadingBlocker(false);
            });
    };

    getUsernames = () => {
        axios.get(config.usersAddress + 'usernames')
            .then(response => {
                if (response.data.length === 0) {
                    this.props.showLoadingBlocker(false);
                    this.props.showErrorMessage('Empty usernames select');
                }
                this.setState({
                    usernames: response.data.map(username => {
                        return {'value': username, 'label': username};
                    })
                });
            });
    };

    getGroupNames = () => {
        axios.get(config.usersAddress + 'groupNames')
            .then(response => {
                if (response.data.length === 0) {
                    this.props.showLoadingBlocker(false);
                    this.props.showErrorMessage('Empty group names select');
                }
                this.setState({
                    groupNames: response.data.map(groupName => {
                        return {'value': groupName, 'label': groupName};
                    })
                });
            });
    };

    render() {
        const { classes } = this.props;

        if (this.state.usernames.length !== 0 && this.state.groupNames.length !== 0 && this.state.events.length !== 0) {
            //todo: redux
            this.props.showLoadingBlocker(false);
        }

        return (
            <div style={{height: '600px'}}>
                {this.state.showDetails &&
                <Modal
                    className={classes.Modal}
                    show={this.state.showDetails}
                    onHide={this.handleCloseEvent}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.eventDetails.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <b>Description:</b> {this.state.eventDetails.description}<br/>
                        <b>Type:</b> {this.state.eventDetails.type}<br/>
                        <b>Date:</b> {this.state.eventDetails.startDate.toLocaleString()} - {this.state.eventDetails.endDate.toLocaleString()}<br/>
                        <b><p>Users:</p></b> <ul>{this.state.eventDetails.users.map(user => <li>{user}</li>)}</ul>
                        <b><p>Groups:</p></b> <ul>{this.state.eventDetails.groups.map(group => <li>{group}</li>)}</ul>
                    </Modal.Body>
                    {this.state.eventDetails.deletionAllowed &&
                    <Modal.Footer>
                        <Button
                            type="submit"
                            onClick={this.deleteEvent}
                        >
                            Delete
                        </Button>
                    </Modal.Footer>}
                </Modal>
                }

                <div style={{width: '700px'}}>
                    <Select
                        className={classes.Select}
                        placeholder='Users or groups'
                        onChange={this.handleFilterTypeChange}
                        autosize={false}
                        value={this.state.filterTypeSelect}
                        options={filterTypes}
                    />

                    {this.state.filterType !== "" &&
                        (this.state.filterType === 'users' ?

                            <Select
                                className={classes.Select}
                                placeholder=''
                                onChange={this.handleFilterValueChange}
                                value={this.state.filterUsersSelect}
                                options={this.state.usernames}
                            />
                            :
                            <Select
                                className={classes.Select}
                                placeholder=''
                                onChange={this.handleFilterValueChange}
                                value={this.state.filterGroupsSelect}
                                options={this.state.groupNames}
                            />
                        )
                    }
                </div>

                <BigCalendar
                    style={{zIndex: -999999}}
                    events={this.state.events}
                    popup={true}
                    onSelectEvent={event => this.handleShowEvent(event)}
                    showMultiDayTimes
                    onRangeChange={time => console.log(time)}
                    defaultDate={new Date()}
                    localizer={localizer}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    showLoadingBlocker,
    showSuccessMessage,
    showErrorMessage
}, dispatch);

const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Calendar))